import { Popover } from "@headlessui/react";
import { DotsVerticalIcon, LightningBoltIcon } from "@heroicons/react/outline";
import { MinusSmIcon, PlusSmIcon } from "@heroicons/react/solid";
import { ReactNode, useState } from "react";

function App() {
  const gameStates = ["pending", "active", "finished", "charachter"];
  const [gameState, setGameState] = useState<string>(gameStates[0]);
  const [maxRounds, setMaxRounds] = useState<number>(20);
  const [round, setRound] = useState<number>(1);
  const [isFated, setIsFated] = useState<boolean>(false);
  const [displayFateState, setDisplayFateState] = useState<boolean>(false);
  const [villain, setVillain] = useState<string>("");
  // State for checkboxes of sets
  const [baseChecked, setBaseChecked] = useState<boolean>(true);
  const [wickedChecked, setWickedChecked] = useState<boolean>(false);
  const [wretchedChecked, setWretchedChecked] = useState<boolean>(false);
  const [preparedChecked, setPreparedChecked] = useState<boolean>(false);
  const [despicableChecked, setDespicableChecked] = useState<boolean>(false);
  const [biggerChecked, setBiggerChecked] = useState<boolean>(false);

  function checkFateState(): void {
    const fate = Math.floor(Math.random() * 10) + 1;
    if (fate >= 1 && fate <= 4) {
      setIsFated(true);
    } else {
      setIsFated(false);
    }
    setDisplayFateState(true);
  }

  function startNewRound(): void {
    setDisplayFateState(false);
    setRound(round + 1);
  }

  function resetGame(): void {
    setGameState(gameStates[0]);
    setRound(1);
    setMaxRounds(20);
    setIsFated(false);
    setDisplayFateState(false);
  }

  function selectVillain(): void {
    const baseVillains = [
      "Captian Hook",
      "Maleficent",
      "Jafar",
      "Ursula",
      "Prince John",
      "Queen of Hearts"
    ];
    const wickedVillains = ["The Evil Queen", "Hades", "Dr Facilier"];
    const wretchedVillains = ["Cruella De Vil", "Mother Gothel", "Pete"];
    const preparedVillains = ["Scar", "Ratigan", "Yzma"];
    const despicableVillains = ["Gaston", "The Horned King", "Lady Tremaine"];
    const biggerVillains = ["Lotso", "Madam Mim", "Syndrome"];

    const villains = [
      ...(baseChecked ? baseVillains : []),
      ...(wickedChecked ? wickedVillains : []),
      ...(wretchedChecked ? wretchedVillains : []),
      ...(preparedChecked ? preparedVillains : []),
      ...(despicableChecked ? despicableVillains : []),
      ...(biggerChecked ? biggerVillains : [])
    ];

    const randomVillain = villains[Math.floor(Math.random() * villains.length)];
    setVillain(randomVillain);
  }

  return (
    <div className="bg-black text-white antialiased h-screen overflow-hidden relative">
      <Popover className="absolute top-2 right-2">
        <Popover.Button className="p-3">
          <DotsVerticalIcon className="w-6 h-6" />
        </Popover.Button>
        <Popover.Panel className="absolute right-2 z-10">
          {({ close }) => (
            <div className="whitespace-nowrap bg-white text-black text-sm rounded-lg py-3">
              {gameState === "pending" ? (
                <>
                  <button
                    onClick={() => {
                      setGameState(gameStates[3]);
                      close();
                    }}
                    className="hover:bg-slate-100 py-1 px-3 w-full text-left"
                  >
                    Choose a villain
                  </button>
                  <div className="px-3 mt-2 pt-2 border-t">
                    <label
                      htmlFor="maxRounds"
                      className="text-xs text-slate-600"
                    >
                      Number of rounds
                    </label>
                    <div className="flex mt-1">
                      <button
                        onClick={() =>
                          maxRounds > 1 && setMaxRounds(maxRounds - 1)
                        }
                        className="flex-shrink-0 flex-grow-0 p-2 border border-gray-200 rounded-l-md"
                      >
                        <span className="sr-only">
                          Decrease number of rounds
                        </span>
                        <MinusSmIcon className="w-5 h-5" />
                      </button>
                      <input
                        id="maxRounds"
                        className="text-center w-12 border-t border-b rounded-none border-gray-200"
                        type="text"
                        value={maxRounds}
                        readOnly
                      />
                      <button
                        onClick={() => setMaxRounds(maxRounds + 1)}
                        className="flex-shrink-0 flex-grow-0 p-2 border border-gray-200 rounded-r-md"
                      >
                        <span className="sr-only">
                          Increase number of rounds
                        </span>
                        <PlusSmIcon className="w-5 h-5" />
                      </button>
                    </div>
                  </div>
                </>
              ) : (
                <button
                  className="hover:bg-slate-100 py-1 px-3 w-full text-left"
                  onClick={() => {
                    resetGame();
                    close();
                  }}
                >
                  Go to main screen
                </button>
              )}
            </div>
          )}
        </Popover.Panel>
      </Popover>

      <div className="flex flex-col min-h-screen justify-between">
        {gameState === "pending" && (
          <>
            <div className="p-8 pb-5">
              <Heading as={"h1"}>Disney Villainous Solo</Heading>
              <Text>
                Your goal is to complete your villain objective before{" "}
                {maxRounds} rounds are completed. If you want to make the game
                easier or harder change the number of rounds in the menu.
              </Text>
              <Text muted={true} small={true}>
                If you play as Maleficent, Lady Tremaine, Yzma, Madam Mim,
                Syndrome or Gaston please read the special rules regarding these
                villains. Complete rules can be found at{" "}
                <a
                  className="underline text-white"
                  target="_blank"
                  href="https://www.disneyexperience.com/news/how-to-play-disney-villainous-solo/"
                >
                  The Disney Experience
                </a>
                .
              </Text>
            </div>
            <div className="p-8 pt-5">
              <Button onClick={() => setGameState(gameStates[1])}>
                Start playing
              </Button>
            </div>
          </>
        )}
        {gameState === "active" && (
          <>
            <div className="p-8 pb-5">
              <Heading as={"h2"}>Round {round}</Heading>
              {displayFateState ? (
                <>
                  {isFated ? (
                    <>
                      <Heading as={"h3"}>
                        Fate has something in store for you
                      </Heading>
                      <Text>
                        Draw two fate cards from your fate deck and play the
                        most devestating card in the most devestating location,
                        regardless of the cards in your hand.
                      </Text>
                    </>
                  ) : (
                    <>
                      <Heading as={"h3"}>Fate did not intervene</Heading>
                      <Text>
                        Proceed to the next round without drawing any fate
                        cards.
                      </Text>
                    </>
                  )}
                </>
              ) : (
                <>
                  <Text>
                    Play your round by moving to a new location and taking as
                    many actions as you wish of the available ones.
                  </Text>
                  <Text small={true} muted={true}>
                    Remember: Condition cards are played when conditions are met
                    by you or the Phantom player.
                  </Text>
                </>
              )}
            </div>
            <div className="p-8 pt-5">
              {displayFateState ? (
                <>
                  {round < maxRounds ? (
                    <Button onClick={() => startNewRound()}>
                      Go to next round
                    </Button>
                  ) : (
                    <Button onClick={() => setGameState(gameStates[2])}>
                      Complete game
                    </Button>
                  )}
                </>
              ) : (
                <Button onClick={() => checkFateState()}>
                  <LightningBoltIcon className="w-6 h-6 mr-1" />
                  <span>Check your fate</span>
                </Button>
              )}
            </div>
          </>
        )}
        {gameState === "finished" && (
          <>
            <div className="p-8">
              <Heading as={"h2"}>Looks like evil didn't win this time</Heading>
              <Text>
                You didn't manage to complete your objective and stop the good
                guys from winning before the end of round {maxRounds}, better
                luck next time.
              </Text>
            </div>
          </>
        )}
        {gameState === "charachter" && (
          <>
            <div className="p-8 pb-0">
              <Heading as={"h2"}>
                Which villain
                <br /> are you?
              </Heading>
              <form className="mt-2">
                <fieldset className="space-y-3">
                  <legend className="sr-only">Select sets</legend>
                  <div className="relative flex items-start">
                    <div className="flex items-center h-5">
                      <input
                        id="base"
                        name="sets"
                        value="base"
                        type="checkbox"
                        className="focus:ring-indigo-500 h-4 w-4 text-slate-700 border-gray-300 rounded"
                        onChange={() => setBaseChecked(!baseChecked)}
                        checked={baseChecked}
                      />
                    </div>
                    <div className="ml-3 text-sm">
                      <label htmlFor="base" className="text-white">
                        The Worst Takes It All
                      </label>
                      <span
                        id="comments-description"
                        className="block text-slate-400 text-xs"
                      >
                        Captian Hook, Maleficent, Jafar, Ursula, Prince John and
                        Queen of Hearts
                      </span>
                    </div>
                  </div>
                  <div className="relative flex items-start">
                    <div className="flex items-center h-5">
                      <input
                        id="wicked"
                        name="sets"
                        value="wicked"
                        type="checkbox"
                        className="focus:ring-indigo-500 h-4 w-4 text-slate-700 border-gray-300 rounded"
                        onChange={() => setWickedChecked(!wickedChecked)}
                        checked={wickedChecked}
                      />
                    </div>
                    <div className="ml-3 text-sm">
                      <label htmlFor="wicked" className="text-white">
                        Wicked to the Core
                      </label>
                      <span
                        id="comments-description"
                        className="block text-slate-400 text-xs"
                      >
                        The Evil Queen, Hades and Dr Facilier
                      </span>
                    </div>
                  </div>
                  <div className="relative flex items-start">
                    <div className="flex items-center h-5">
                      <input
                        id="prepared"
                        name="sets"
                        value="prepared"
                        type="checkbox"
                        className="focus:ring-indigo-500 h-4 w-4 text-slate-700 border-gray-300 rounded"
                        onChange={() => setPreparedChecked(!preparedChecked)}
                        checked={preparedChecked}
                      />
                    </div>
                    <div className="ml-3 text-sm">
                      <label htmlFor="prepared" className="text-white">
                        Evil Comes Prepared
                      </label>
                      <span
                        id="comments-description"
                        className="block text-slate-400 text-xs"
                      >
                        Scar, Ratigan and Yzma
                      </span>
                    </div>
                  </div>
                  <div className="relative flex items-start">
                    <div className="flex items-center h-5">
                      <input
                        id="wretched"
                        name="sets"
                        value="wretched"
                        type="checkbox"
                        className="focus:ring-indigo-500 h-4 w-4 text-slate-700 border-gray-300 rounded"
                        onChange={() => setWretchedChecked(!wretchedChecked)}
                        checked={wretchedChecked}
                      />
                    </div>
                    <div className="ml-3 text-sm">
                      <label htmlFor="wretched" className="text-white">
                        Perfectly Wretched
                      </label>
                      <span
                        id="comments-description"
                        className="block text-slate-400 text-xs"
                      >
                        Cruella De Vil, Mother Gothel and Pete
                      </span>
                    </div>
                  </div>
                  <div className="relative flex items-start">
                    <div className="flex items-center h-5">
                      <input
                        id="despicable"
                        name="sets"
                        value="despicable"
                        type="checkbox"
                        className="focus:ring-indigo-500 h-4 w-4 text-slate-700 border-gray-300 rounded"
                        onChange={() =>
                          setDespicableChecked(!despicableChecked)
                        }
                        checked={despicableChecked}
                      />
                    </div>
                    <div className="ml-3 text-sm">
                      <label htmlFor="despicable" className="text-white">
                        Despicable Plots
                      </label>
                      <span
                        id="comments-description"
                        className="block text-slate-400 text-xs"
                      >
                        Gaston, The Horned King and Lady Tremaine
                      </span>
                    </div>
                  </div>
                  <div className="relative flex items-start">
                    <div className="flex items-center h-5">
                      <input
                        id="bigger"
                        name="sets"
                        value="bigger"
                        type="checkbox"
                        className="focus:ring-indigo-500 h-4 w-4 text-slate-700 border-gray-300 rounded"
                        onChange={() => setBiggerChecked(!biggerChecked)}
                        checked={biggerChecked}
                      />
                    </div>
                    <div className="ml-3 text-sm">
                      <label htmlFor="bigger" className="text-white">
                        Bigger and Badder
                      </label>
                      <span
                        id="comments-description"
                        className="block text-slate-400 text-xs"
                      >
                        Lotso, Madam Mim and Syndrome
                      </span>
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
            <div className="p-8 pt-0">
              {villain !== "" && villain !== undefined && (
                <div className="text-center mb-4">
                  <Text>
                    <span className="block text-sm">You are</span>
                    <span className="text-xl">{villain}</span>
                  </Text>
                </div>
              )}
              <Button onClick={() => selectVillain()}>
                Which villain am I?
              </Button>
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default App;

const Button = ({
  children,
  onClick
}: {
  children: ReactNode;
  onClick: () => void;
}) => {
  return (
    <button
      onClick={onClick}
      className="flex w-full items-center justify-center px-6 py-3 border border-transparent text-lg rounded-xl font-semibold text-black bg-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-slate-500"
    >
      {children}
    </button>
  );
};

const Text = ({
  children,
  muted = false,
  small = false
}: {
  children: ReactNode;
  muted?: boolean;
  small?: boolean;
}) => {
  return (
    <p
      className={`text-white leading-relaxed mt-6 ${
        muted && "text-slate-400"
      } ${small ? "text-sm" : "text-lg"}`}
    >
      {children}
    </p>
  );
};

const Heading = ({ children, as }: { children: ReactNode; as: string }) => {
  const baseClasses = "text-white font-black";
  switch (as) {
    case "h1":
      return <h1 className={`text-5xl ${baseClasses}`}>{children}</h1>;
      break;
    case "h2":
      return <h2 className={`text-4xl ${baseClasses}`}>{children}</h2>;
      break;
    case "h3":
      return <h3 className={`text-3xl ${baseClasses}`}>{children}</h3>;
      break;
    case "h4":
      return <h4 className={`text-2xl ${baseClasses}`}>{children}</h4>;
      break;
    case "h5":
      return <h5 className={`text-xl ${baseClasses}`}>{children}</h5>;
      break;
    case "h6":
      return <h6 className={`text-lg ${baseClasses}`}>{children}</h6>;
      break;
    default:
      return <h2 className={`text-4xl ${baseClasses}`}>{children}</h2>;
      break;
  }
};
